/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

import static axiomsecurityprovider.CipherSpiImpl.provider;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.SignatureSpi;
import java.security.interfaces.RSAPublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.security.Provider;

/**
 *
 * @author Sai
 */

public class SignatureImplementation extends SignatureSpi {

   public static  Provider provider;

   private static Signature sign = null;

    public SignatureImplementation() throws ClassNotFoundException {
        try {
         provider = ProviderToUse.LoadProvider();
         System.out.println(provider.getName());
          } catch (Exception ex) {
              ex.printStackTrace();
            }
    }

    @Override
    protected void engineInitVerify(PublicKey publicKey) throws InvalidKeyException {
        try {
//            if (provider.getName() == "SunJCE") {
                sign = Signature.getInstance("SHA1withRSA",provider);
                   sign.initVerify(publicKey);

//            } else if (provider.getName()== "BC") {
//               sign = Signature.getInstance("SHA1withRSA",provider);
//                    sign.initVerify(publicKey);
//                }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected void engineInitSign(PrivateKey privateKey) throws InvalidKeyException {
        try {
//            if (provider.getName() == "SunJCE") {
               sign = Signature.getInstance("SHA1withRSA",ProviderToUse.ProiderName);
                sign.initSign(privateKey);
                System.out.print("WOW");
//            } else if (provider.getName() == "BC") {
//
//             sign=Signature.getInstance("SHA1withRSA", provider);
//                System.out.print(privateKey);
//                sign.initSign(privateKey);
//                System.out.print("WOW");
//                
        }
         catch (Exception ex) {
            
            ex.printStackTrace();
        }

    }

    @Override
    protected void engineUpdate(byte b) throws SignatureException {

        try
        {
             sign.update(b);
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }

    }

    @Override
    protected void engineUpdate(byte[] b, int off, int len) throws SignatureException {

        try
        {
//        if (provider.getName() == "JCE") {
            sign.update(b, off, len);

//        } else if (provider.getName() == "BC") {
//                
//                sign.update(b, off, len);
//                System.out.print("WOW");
//            } 

        
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
      
    }

    @Override
    protected byte[] engineSign() throws SignatureException {
        
        byte[] signedData = null;
        try
        {
//        if (provider.getName() == "JCE") {
            signedData = sign.sign();
//        } else if (provider.getName() == "BC") {
//
//            signedData = sign.sign();
//        
//
//        }
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return signedData;

    }

    @Override
    protected boolean engineVerify(byte[] sigBytes) throws SignatureException {
        boolean op = false;
        try {
//            if (provider.getName() == "JCE") {
                op = sign.verify(sigBytes);

//            } else if (provider.getName() == "BC") {
//
//                op = sign.verify(sigBytes);
//
//            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return op;

    }

    @Override
    protected void engineSetParameter(String param, Object value) throws InvalidParameterException {

//        if (provider.getName() == "JCE") {
            sign.setParameter(param, value);
//        } else if (provider.getName() == "BC") {
//            try {
//
//                sign.setParameter(param, value);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }

        

     
    }

    @Override
    protected Object engineGetParameter(String param) throws InvalidParameterException {
        Object obj = null;
        try
        {
//        if (provider.getName() == "JCE") {
            obj = sign.getParameter(param);
//        } else if (provider.getName() == "BC") {
//            try {
//
//                obj = sign.getParameter(param);
//
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }

        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        return obj;

    

}
}