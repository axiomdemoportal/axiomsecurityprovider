/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

/**
 *
 * @author Sai
 */ 
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.KeyAgreementSpi;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.KeyAgreement;
import java.security.Provider;
public class KeyAgreementSpiImpl extends KeyAgreementSpi{
Provider provider=null;
public KeyAgreementSpiImpl() throws ClassNotFoundException
        
        
{

    provider=ProviderToUse.LoadProvider();


}
   
    
    @Override
    protected void engineInit(Key key, SecureRandom sr) throws InvalidKeyException {
        
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="jce")
//            {
                KeyAgreement ka=KeyAgreement.getInstance("RSA",ProviderToUse.ProiderName);
                ka.init(key, sr);
                
                
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                KeyAgreement ka=KeyAgreement.getInstance("RSA",provider);
//                
//                ka.init(key, sr);
//                
//                
//            }
            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        
    }

    @Override
    protected void engineInit(Key key, AlgorithmParameterSpec aps, SecureRandom sr) throws InvalidKeyException, InvalidAlgorithmParameterException {
       
        
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="jce")
//            {
                KeyAgreement ka=KeyAgreement.getInstance("RSA",ProviderToUse.ProiderName);
                ka.init(key, aps, sr);
                
                
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                KeyAgreement ka=KeyAgreement.getInstance("RSA",provider);
//                
//                ka.init(key, aps, sr);
//                
//                
//            }
            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        
        
        
        
    }

    @Override
    protected Key engineDoPhase(Key key, boolean bln) throws InvalidKeyException, IllegalStateException {
     
        Key kei=null;
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="jce")
//            {
                KeyAgreement ka=KeyAgreement.getInstance("RSA");
                kei= ka.doPhase(key, bln);
                
                
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                KeyAgreement ka=KeyAgreement.getInstance("RSA",provider);
//                
//               kei= ka.doPhase(key, bln);
//                
//                
//            }
            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        return kei;
        
        
    }

    @Override
    protected byte[] engineGenerateSecret() throws IllegalStateException {
      
       byte[] op=null;
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="jce")
//            {
                KeyAgreement ka=KeyAgreement.getInstance("RSA");
               op=  ka.generateSecret();
                
                
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                KeyAgreement ka=KeyAgreement.getInstance("RSA",provider);
//                
//               op= ka.generateSecret();
//                
//                
//            }
            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        return op;
        
        
         }

    @Override
    protected int engineGenerateSecret(byte[] bytes, int i) throws IllegalStateException, ShortBufferException {
  
        int op=0;
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="jce")
//            {
                KeyAgreement ka=KeyAgreement.getInstance("RSA");
              op=    ka.generateSecret(bytes, i);
                
                
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                KeyAgreement ka=KeyAgreement.getInstance("RSA",provider);
//                
//               op= ka.generateSecret(bytes, i);
//                
//                
//            }
            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        return op;
        
        
    }

    @Override
    protected SecretKey engineGenerateSecret(String string) throws IllegalStateException, NoSuchAlgorithmException, InvalidKeyException {
    
        SecretKey kei=null;
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="jce")
//            {
                KeyAgreement ka=KeyAgreement.getInstance("RSA");
                 kei= ka.generateSecret(string);
                
                
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                KeyAgreement ka=KeyAgreement.getInstance("RSA",provider);
//                
//               kei=ka.generateSecret(string);
//                
//                
//            }
            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        return kei;
        
        
        
    }
    
    
}
