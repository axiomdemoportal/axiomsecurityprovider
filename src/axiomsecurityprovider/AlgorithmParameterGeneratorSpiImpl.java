/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

import java.security.AlgorithmParameterGenerator;
import java.security.AlgorithmParameterGeneratorSpi;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Provider;

/**
 *
 * @author Sai
 */
public class AlgorithmParameterGeneratorSpiImpl extends AlgorithmParameterGeneratorSpi  {

    Provider provider= null;     //ProviderToUse.LoadProvider();
    
    @Override
    protected void engineInit(int size, SecureRandom random) {
         try
        {
        
     AlgorithmParameterGenerator ag= AlgorithmParameterGenerator.getInstance("RSA",ProviderToUse.SelectedProvider);
                ag.init(size, random);
        
        }catch(Exception ex)
        {
        
            ex.printStackTrace();
        
        }
       
  }

    @Override
    protected void engineInit(AlgorithmParameterSpec genParamSpec, SecureRandom random) throws InvalidAlgorithmParameterException {
       try
       {
       
     AlgorithmParameterGenerator ag= AlgorithmParameterGenerator.getInstance("RSA",ProviderToUse.ProiderName);
                ag.init(genParamSpec, random);
        
       }
      catch(Exception ex)
      {
          ex.printStackTrace();
      }
       
      }

    @Override
    protected AlgorithmParameters engineGenerateParameters() {
        
        
        try
        {
      
    AlgorithmParameterGenerator ag= AlgorithmParameterGenerator.getInstance("RSA",ProviderToUse.ProiderName);
                ag.generateParameters();
        
        }catch(Exception ex)
        {
    ex.printStackTrace();
       }
        return null;
       
        
        }
    
    
  
    
    
    
}
