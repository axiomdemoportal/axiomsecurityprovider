/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.SecureRandomSpi;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.Provider;

/**
 *
 * @author Sai
 */
public class SecureRandomSpiImpl extends SecureRandomSpi {

    Provider provider=null;
    public SecureRandomSpiImpl() throws ClassNotFoundException
     {
         provider=ProviderToUse.LoadProvider();
         
    }
    @Override
    protected void engineSetSeed(byte[] seed) {
        
        
//        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//        {
            try {
                SecureRandom sr= SecureRandom.getInstance("SHA1PRNG",ProviderToUse.ProiderName);
                sr.setSeed(seed);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(SecureRandomSpiImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
          
//        }
//        
//        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//        {
//            try
//            {
//             SecureRandom sr=SecureRandom.getInstance("SHA1PRNG",provider);
//             
//             sr.setSeed(seed);
//            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
//        }
//        
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void engineNextBytes(byte[] bytes) {
        
//        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//        {
            try {
                SecureRandom sr= SecureRandom.getInstance("SHA1PRNG",ProviderToUse.ProiderName);
                sr.nextBytes(bytes);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(SecureRandomSpiImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
          
        
        
////        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
////        {
//            try
//            {
//             SecureRandom sr=SecureRandom.getInstance("SHA1PRNG",provider);
//             
//             sr.nextBytes(bytes);
//            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        
    
    
        
    }
        
        
        
        
     
    

    @Override
    protected byte[] engineGenerateSeed(int numBytes) {
    byte[] op=null;
        
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//        {
            try {
                SecureRandom sr= SecureRandom.getInstance("SHA1PRNG",ProviderToUse.ProiderName);
              op=  sr.generateSeed(numBytes);
                      } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(SecureRandomSpiImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
          
//        }
//        
//        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//        {
//            try
//            {
//             SecureRandom sr=SecureRandom.getInstance("SHA1PRNG",provider);
//             
//           op=sr.generateSeed(numBytes);
//            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
//           
//            
//
//        }
                  return op;
    
        
        
        
        
        
        
        
    }
    
}
