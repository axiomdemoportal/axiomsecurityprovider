/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.KeyStoreSpi;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.Enumeration;

/**
 *
 * @author Sai
 */
public class KeyStoreSpiImpl extends KeyStoreSpi {

    Provider provider = null;
    public static KeyStore ks = null;
    String CertPath = "";
    String keyStoreType = null;

    //File file= new File(path);
    //ProviderToUse.LoadProvider();

    public KeyStoreSpiImpl() {

        try {
            provider = ProviderToUse.LoadProvider();
         
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//    public void LoadKeyStore() throws KeyStoreException, FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException {
//
//        File f = new File(path);
//        FileInputStream fin = new FileInputStream(f);
//        ks = KeyStore.getInstance("JCEKS");
//        ks.load(fin, "blue3800".toCharArray());
//
//    }

    @Override
    public Key engineGetKey(String alias, char[] password) throws NoSuchAlgorithmException, UnrecoverableKeyException {
        String pass=ProviderToUse.keyStorePassWord;
        Key k = null;
        try {

            k = ks.getKey(alias, pass.toCharArray());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return k;
    }

    @Override
    public Certificate[] engineGetCertificateChain(String alias) {

        Certificate[] cchain = null;
        Certificate[] ch = null;

        try {

            cchain = ks.getCertificateChain(alias);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return cchain;

    }

    @Override
    public Certificate engineGetCertificate(String alias) {

        Certificate cf = null;

        try {

            cf = ks.getCertificate(alias);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return cf;

    }

    @Override
    public Date engineGetCreationDate(String alias) {

        Date dt = null;

        try {

            dt = ks.getCreationDate(alias);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return dt;

    }

    @Override
    public void engineSetKeyEntry(String alias, Key key, char[] password, Certificate[] chain) throws KeyStoreException {
       String pass=ProviderToUse.keyStorePassWord;
        String KeyId = null;
        
        try {
            //ks=KeyStore.getInstance("JCEKS");
            ks.setKeyEntry(alias, key, pass.toCharArray(), chain);
         
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void engineSetKeyEntry(String alias, byte[] key, Certificate[] chain) throws KeyStoreException {

//        if (provider.getName() == "SunJCE") {
            try {
                //  ks=KeyStore.getInstance("JCEKS");
              ks.setKeyEntry(alias, key, chain);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        

    }

    @Override
    public void engineSetCertificateEntry(String alias, Certificate cert) throws KeyStoreException {

        try {
//       if(provider.getName()=="SunJCE")
//        {
//      
          //  File f = new File(path);

//        ks.load(fin,"blue3800".toCharArray());
          //  FileOutputStream fout = new FileOutputStream(f);
            ks.setCertificateEntry(alias, cert);
          //  ks.store(fout, "blue3800".toCharArray());

//        }
//        if(provider.getName()=="BC")
//        {
//         
//        ks=KeyStore.getInstance("JCEKS",provider);
//        ks.load(null, null);
//         ks.setCertificateEntry(alias, cert);
//        }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void engineDeleteEntry(String alias) throws KeyStoreException {

        try {
//    if(provider.getName()=="SunJCE")
//        {

            //  ks=KeyStore.getInstance("JCEKS",provider);
            ks.deleteEntry(alias);

//        }
//        if(provider.getName()=="BC")
//        {
//         
//        
//        ks=KeyStore.getInstance("JCEKS",provider);
//         ks.deleteEntry(alias);
//        }
//        
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public Enumeration<String> engineAliases() {
        Enumeration<String> eAlias = null;

        try {
//        if(provider.getName()=="SunJCE")
//        {

            // ks =KeyStore.getInstance("JCEKS",provider);
            eAlias = ks.aliases();

//        }
//        if(provider.getName()=="BC")
//        {
//        
//        ks=KeyStore.getInstance("JCEKS",provider);
//         eAlias=ks.aliases();
//         }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return eAlias;

    }

    @Override
    public boolean engineContainsAlias(String alias) {

        boolean op = false;
        try {

//       if(provider.getName()=="SunJCE")
//        {
//       
//        
            //  ks=KeyStore.getInstance("JCEKS",provider);
            op = ks.containsAlias(alias);

//       
//        }
//        if(provider.getName()=="BC")
//        {
//       
//        ks=KeyStore.getInstance("JCEKS",provider);
//         op=ks.containsAlias(alias);
//        
//           
//    
//      
//        }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return op;

    }

    @Override
    public int engineSize() {

        int op = 0;
        try {
//           if(provider.getName()=="SunJCE")
//        {
//       
            //  ks =KeyStore.getInstance("JCEKS",provider);
            op = ks.size();

//        }
//        if(provider.getName()=="BC")
//        {
//        
//        
//        ks=KeyStore.getInstance("JCEKS",provider);
//         op=ks.size();
//       
            // }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return op;

    }

    @Override
    public boolean engineIsKeyEntry(String alias) {

        boolean op = false;

        try {
//        if(provider.getName()=="SunJCE")
//        {

            // ks =KeyStore.getInstance("JCEKS",provider);
            op = ks.isKeyEntry(alias);

//        }
//        if(provider.getName()=="BC")
//        {
//       
//         ks=KeyStore.getInstance("JCEKS",provider);
//          op=ks.isKeyEntry(alias);
//        }
//       
//    
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return op;

    }

    @Override
    public boolean engineIsCertificateEntry(String alias) {

        boolean op = false;

        try {
//       if(provider.getName()=="SunJCE")
//        {
            //   ks =KeyStore.getInstance("JCEKS",provider);
            op = ks.isCertificateEntry(alias);
//        }
//       
//        
//        if(provider.getName()=="BC")
//        {
//       
//       ks=KeyStore.getInstance("JCEKS",provider);
//         op=ks.isCertificateEntry(alias);
//        }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return op;

    }

    @Override
    public String engineGetCertificateAlias(Certificate cert) {

        String op = null;

        try {
//       if(provider.getName()=="SunJCE")
//        {

            //ks=KeyStore.getInstance("JCEKS",provider);
            op = ks.getCertificateAlias(cert);

//        }
//        if(provider.getName()=="BC")
//        {
//         
//      ks=KeyStore.getInstance("JCEKS",provider);
//         op=ks.getCertificateAlias(cert);
//        }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return op;

    }

    @Override
    public void engineStore(OutputStream stream, char[] password) throws IOException, NoSuchAlgorithmException, CertificateException {
        {
     try {
         File file = new File(ProviderToUse.keyStoreFilePath);
        
        String pass=ProviderToUse.keyStorePassWord;
         FileOutputStream fout= new FileOutputStream(file);
         if(provider.getName()=="SunJCE")
       {
       
//           ks =KeyStore.getInstance("JCEKS",provider);
           ks.store(fout, pass.toCharArray());
//
////        
       }
       if(provider.getName()=="BC")
         {
////   
     //  ks=KeyStore.getInstance("JCEKS",provider);
         ks.store(fout, pass.toCharArray());
    
        } 

    }
     catch (Exception ex) {
            ex.printStackTrace();
        }
        }}
    @Override
    public void engineLoad(InputStream stream, char[] password) throws IOException, NoSuchAlgorithmException, CertificateException {
        File file = new File(ProviderToUse.keyStoreFilePath);
        
        String pass=ProviderToUse.keyStorePassWord;
       
       
        try {
           if(provider.getName()=="SunJCE")
          {
              if(file.exists())
              {
               FileInputStream fin = new FileInputStream(file); 
                ks = KeyStore.getInstance("JCEKS", provider);
            ks.load(fin,pass.toCharArray());
              }
              else
             {
            ks = KeyStore.getInstance("JCEKS", provider);
            ks.load(null,pass.toCharArray());
            FileOutputStream fout= new FileOutputStream(file);
              ks.store(fout, password);
                  
                  
              }
           
          }
         if(provider.getName()=="BC")
         {
             if(file.exists())
             { 
                 FileInputStream fin = new FileInputStream(file); 
           ks = KeyStore.getInstance("JCEKS");
            ks.load(fin,pass.toCharArray());
//         ks=KeyStore.getInstance("BKS",provider);
//         ks.load(stream, password);
             }
             else
             {
            ks = KeyStore.getInstance("JCEKS");
            ks.load(null,pass.toCharArray());
            FileOutputStream fout= new FileOutputStream(file);
              ks.store(fout, pass.toCharArray());
                 
             }
         }
        
        if(provider.getName()=="SAFENET.3")
        {
            ks=KeyStore.getInstance("CRYPTOKI",provider);
            ks.load(stream, password);
        }
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
