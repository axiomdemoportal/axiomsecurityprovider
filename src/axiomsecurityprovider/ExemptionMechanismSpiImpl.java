/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package axiomsecurityprovider; 
/**
 *
 * @author Sai
 */
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.ExemptionMechanismException;
import javax.crypto.ExemptionMechanismSpi;
import javax.crypto.ShortBufferException;
import javax.crypto.ExemptionMechanism;
import java.security.Provider;
public class ExemptionMechanismSpiImpl extends ExemptionMechanismSpi{

    Provider provider=null;
    
    public ExemptionMechanismSpiImpl() throws ClassNotFoundException
    {
        provider=ProviderToUse.LoadProvider();
    }
    
    @Override
    protected int engineGetOutputSize(int i) {
        int op=0;
        try
        {
            
//            if(provider==null)
//            {
          ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",ProviderToUse.ProiderName);
        op= ex.getOutputSize(i);
//        }
//          
//            if(provider.getName()=="BC")
//            {
//        ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",provider);
//        op= ex.getOutputSize(i);
//                
//            }
        }
       
        catch(Exception ex)
        {
            ex.printStackTrace();;
        }
        
        return op;
    }

    @Override
    protected void engineInit(Key key) throws InvalidKeyException, ExemptionMechanismException {
        try
        {
            
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
          ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",ProviderToUse.ProiderName);
         ex.init(key);
//        }
//          
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",provider);
//         ex.init(key);
//                
//            }
        }
       
        catch(Exception ex)
        {
            ex.printStackTrace();;
        }
        
 
       
     
        
        
        
    }

    @Override
    protected void engineInit(Key key, AlgorithmParameterSpec aps) throws InvalidKeyException, InvalidAlgorithmParameterException, ExemptionMechanismException {
  
               try
        {
            
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
          ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA");
//         ex.init(key, aps);
//        }
//          
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",provider);
//        ex.init(key, aps);
//                
//            }
        }
       
        catch(Exception ex)
        {
            ex.printStackTrace();;
        }
        
       
 
        
        
        
        
        
    }

    @Override
    protected void engineInit(Key key, AlgorithmParameters ap) throws InvalidKeyException, InvalidAlgorithmParameterException, ExemptionMechanismException {
       
               try
        {
            
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
          ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",ProviderToUse.ProiderName);
         ex.init(key, ap);
//        }
//          
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",provider);
//         ex.init(key, ap);
//                
//            }
        }
       
        catch(Exception ex)
        {
            ex.printStackTrace();;
        }
        
        
        
        
    }

    @Override
    protected byte[] engineGenExemptionBlob() throws ExemptionMechanismException {
     byte[] op=null;
     try
     {
//         if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//         {
                ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",ProviderToUse.ProiderName);
             op=ex.genExemptionBlob();
             
//         }
//         
//             if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",provider);
//         ex.genExemptionBlob();
//                
//            }
        
         
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
        
       return op;
        
    }

    @Override
    protected int engineGenExemptionBlob(byte[] bytes, int i) throws ShortBufferException, ExemptionMechanismException {
       
     int op=0;
     try
     {
//         if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//         {
                ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",ProviderToUse.ProiderName);
             op=ex.genExemptionBlob(bytes, i);
             
//         }
//         
//             if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        ExemptionMechanism ex=ExemptionMechanism.getInstance("RSA",provider);
//         op=  ex.genExemptionBlob(bytes, i);
//                
//            }
        
         
     }
     catch(Exception ex)
     {
         ex.printStackTrace();
     }
        
       return op;
        
        
        
        
    
    }
    
}
