/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

import java.io.InputStream;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateFactorySpi;
import java.util.Collection;
import java.security.Provider;
/**
 *
 * @author Sai
 */
public class CertificateFactorySpiImpl extends CertificateFactorySpi {

    Provider provider;
    
   public CertificateFactorySpiImpl() throws ClassNotFoundException
   {
       provider=ProviderToUse.LoadProvider();
   }
    
    @Override
    public Certificate engineGenerateCertificate(InputStream inStream) throws CertificateException {
      
        Certificate cer=null;
       try
     {
//        if(provider==null)
//        {
        CertificateFactory cert= CertificateFactory.getInstance("X.509",ProviderToUse.ProiderName);
             cer=  cert.generateCertificate(inStream);
       // }
     
//        else   if(provider.getName()=="BouncyCastle")
//        {
//      CertificateFactory cert= CertificateFactory.getInstance("X.509",provider);
//              cer= cert.generateCertificate(inStream);
//        }
     
      }catch(Exception ex)
      {
          ex.printStackTrace();
      }
        
        return cer;
        
   
    }

    @Override
    public Collection<? extends Certificate> engineGenerateCertificates(InputStream inStream) throws CertificateException {
       Collection<? extends Certificate> p=null;
        
           try
      {
//        if(provider==null)
//        {
        CertificateFactory cert= CertificateFactory.getInstance("X.509",ProviderToUse.ProiderName);
             p=  cert.generateCertificates(inStream);
//        }
//     
//        else   if(provider.getName()=="BouncyCastle")
//        {
//      CertificateFactory cert= CertificateFactory.getInstance("X.509",provider);
//              p= cert.generateCertificates(inStream);
//        }
     
      }   catch(Exception ex)
      {
          ex.printStackTrace();
      }
     
    
    return p;
        
        
        
   
    }

    @Override
    public CRL engineGenerateCRL(InputStream inStream) throws CRLException {
       CRL c=null;
        
             try
      {
//        if(provider==null)
//        {
        CertificateFactory cert= CertificateFactory.getInstance("X.509",ProviderToUse.ProiderName);
             c=  cert.generateCRL(inStream);
//        }
//     
//        else   if(provider.getName()=="BouncyCastle")
//        {
//      CertificateFactory cert= CertificateFactory.getInstance("X.509",provider);
//              c=cert.generateCRL(inStream);
//        }
     
      }   catch(Exception ex)
      {
          ex.printStackTrace();
      }
        
        return c;
        
        
        
    }

    @Override
    public Collection<? extends CRL> engineGenerateCRLs(InputStream inStream) throws CRLException {
        Collection<? extends CRL> cl=null;
        
             try
      {
//        if(provider==null)
//        {
        CertificateFactory cert= CertificateFactory.getInstance("X.509",ProviderToUse.ProiderName);
             cl=  cert.generateCRLs(inStream);
//        }
//     
//        else   if(provider.getName()=="BouncyCastle")
//        {
//      CertificateFactory cert= CertificateFactory.getInstance("X.509",provider);
//              cl=cert.generateCRLs(inStream);
//        }
     
      }   catch(Exception ex)
      {
          ex.printStackTrace();
      }
        
        return cl;
        
      
    }
    
   
    
    
    
    
}
