/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

/**
 *
 * @author Sai
 */
import java.security.Provider;

public class AxiomSecurityProvider extends Provider {

    private static final String OID_PKCS12_RC2_40 = "1.2.840.113549.1.12.1.6";
    private static final String OID_PKCS12_DESede = "1.2.840.113549.1.12.1.3";
    private static final String OID_PKCS5_MD5_DES = "1.2.840.113549.1.5.3";
    private static final String OID_PKCS5_PBKDF2 = "1.2.840.113549.1.5.12";
    private static final String OID_PKCS3 = "1.2.840.113549.1.3.1";
    public static String SelectedProvider = "JCE";
   
    public AxiomSecurityProvider() {
        super("AxiomSecurityProvider", 1.0, "AxiomSecurityProvider provides interface for HSM");
      //  put("Signature.SHA1withDSA", "sun.security.provider.DSA");
        put("KeyGenerator.DESede", "sun.security.provider.DSA");
        put("KeyGenerator.AES", "axiomsecurityprovider.KeyGeneratorSpiImpl");
       
        put("KeyPairGenerator.RSA", "axiomsecurityprovider.KeyPairGeneratorSpiImpl");
        put("KeyGenerator.AES", "axiomsecurityprovider.KeyGeneratorSpiImpl");
         put("KeyGenerator.DES", "axiomsecurityprovider.KeyGeneratorDesSpi");
        put("KeyGenerator.DES", "axiomsecurityprovider.KeyGeneratorSpiImpl");
        put("AlgorithmParameterGenerator.RSA", "axiomsecurityprovider.AlgorithmParameterGeneratorSpiImpl");
        put("AlgorithmParameters.RSA", "axiomsecurityprovider.AlgorithmParametersSpiImpl");
        put("CertificateFactory.RSA", "axiomsecurityprovider.CertificateFactorySpiImpl");
        put("Cipher.RSA", "axiomsecurityprovider.CipherSpiImpl");
        put("Cipher.AES", "axiomsecurityprovider.CipherSpiImplAES");
        put("Cipher.DES", "axiomsecurityprovider.CipherSpiDesImpl");
        put("Signature.SHA1withRSA", "axiomsecurityprovider.SignatureImplementation");  
        //put("Cipher.RSA/ECB/PKCS5Padding", "axiomsecurityprovider.CipherSpiImpl");
       // put("Cipher.RSA/ECB/PKCS1Padding", "axiomsecurityprovider.CipherSpiImpl");
//        put("Cipher.RSA SupportedModes", "ECB");
//        put("Cipher.RSA SupportedModes", "CBC");
          put("KeyStore.JCEKS", "axiomsecurityprovider.KeyStoreSpiImpl");
//        put("Cipher.RSA SupportedPaddings", "NOPADDING|PKCS1PADDING|OAEPWITHMD5ANDMGF1PADDING"
//               + "|OAEPWITHSHA1ANDMGF1PADDING"
//                + "|OAEPWITHSHA-1ANDMGF1PADDING"
//               + "|OAEPWITHSHA-256ANDMGF1PADDING"
//               + "|OAEPWITHSHA-384ANDMGF1PADDING"
//                + "|OAEPWITHSHA-512ANDMGF1PADDING");
        put("ExemptionMechanism.RSA", "axiomsecurityprovider.ExemptionMechanismSpiImpl");
        put("KeyAgreement.RSA", "axiomsecurityprovider.KeyAgreementSpiImpl");
        put("KeyFactory.RSA", "axiomsecurityprovider.KeyFactoryImpl");
        put("KeyGenerator.RSA", "axiomsecurityprovider.KeyGeneratorSpiImpl");
        put("KeyStore.RSA", "axiomsecurityprovider.KeyStoreSpiImpl");
        put("Mac.RSA", "axiomsecurityprovider.MacSpiImpl");
        put("SecretKeyFactory.RSA", "axiomsecurityprovider.SecretKeyFactorySpiImpl");
        put("SecureRandom.RSA", "axiomsecurityprovider.SecureRandomSpiImpl");
      


        put("AlgorithmParameterGenerator.DiffieHellman",
                "com.sun.crypto.provider.DHParameterGenerator");
        put("Alg.Alias.AlgorithmParameterGenerator.DH",
                "DiffieHellman");
        put("Alg.Alias.AlgorithmParameterGenerator.OID." + OID_PKCS3,
                "DiffieHellman");
        put("Alg.Alias.AlgorithmParameterGenerator." + OID_PKCS3, "DiffieHellman");


        put("KeyAgreement.DiffieHellman",
                "com.sun.crypto.provider.DHKeyAgreement");
        put("Alg.Alias.KeyAgreement.DH", "DiffieHellman");
        put("Alg.Alias.KeyAgreement.OID." + OID_PKCS3, "DiffieHellman");
        put("Alg.Alias.KeyAgreement." + OID_PKCS3, "DiffieHellman");

        put("KeyAgreement.DiffieHellman SupportedKeyClasses",
                "javax.crypto.interfaces.DHPublicKey"
                + "|javax.crypto.interfaces.DHPrivateKey");







    }
    
   
     public static void main(String[] args) {
       
        
          
        
          }
}
