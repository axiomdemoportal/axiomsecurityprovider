/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

/**
 *
 * @author Sai
 */
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.MacSpi;
import javax.crypto.Mac;
import java.security.Provider;
public class MacSpiImpl extends MacSpi{

    Provider provider=null;
    public MacSpiImpl() throws ClassNotFoundException
    {
        provider=ProviderToUse.LoadProvider();
        
    }
    
    @Override
    protected int engineGetMacLength() {
        
        int op=0;
        
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
        Mac mc=Mac.getInstance("HmacSHA1",ProviderToUse.ProiderName);
        op=mc.getMacLength();
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        Mac mc=Mac.getInstance("HmacSHA1",provider);
//        op=mc.getMacLength();          
//                
//                
//            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return op;

    }

    @Override
    protected void engineInit(Key key, AlgorithmParameterSpec aps) throws InvalidKeyException, InvalidAlgorithmParameterException {
  
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
        Mac mc=Mac.getInstance("HmacSHA1",ProviderToUse.ProiderName);
        mc.init(key, aps);
//                }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        Mac mc=Mac.getInstance("HmacSHA1",provider);
//           mc.init(key, aps);
//                
//                
//            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        
        
        
        
    }

    @Override
    protected void engineUpdate(byte b) {
    
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
        Mac mc=Mac.getInstance("HmacSHA1",ProviderToUse.ProiderName);
        mc.update(b);
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        Mac mc=Mac.getInstance("HmacSHA1",provider);
//        mc.update(b);
//                
//                
//            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        
        
    }

    @Override
    protected void engineUpdate(byte[] bytes, int i, int i1) {
   
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
          Mac mc=Mac.getInstance("HmacSHA1",ProviderToUse.ProiderName);
            mc.update(bytes, i, i1);
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        Mac mc=Mac.getInstance("HmacSHA1",provider);
//         mc.update(bytes, i, i1);
//                
//                
//            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
    }

    @Override
    protected byte[] engineDoFinal() {
     
        byte[] op=null;
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
        Mac mc=Mac.getInstance("HmacSHA1",ProviderToUse.ProiderName);
        op=  mc.doFinal();
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        Mac mc=Mac.getInstance("HmacSHA1",provider);
//        op=  mc.doFinal();
//                
//                
//            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        return op;
    }

    @Override
    protected void engineReset() {
 
        
      
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
        Mac mc=Mac.getInstance("HmacSHA1",ProviderToUse.ProiderName);
        mc.reset();
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//        Mac mc=Mac.getInstance("HmacSHA1",provider);
//         mc.reset();
//                
//                
//            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
      
        
        
    }
    
}
