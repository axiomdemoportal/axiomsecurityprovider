/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

import java.security.InvalidAlgorithmParameterException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 *
 * @author Sai
 */
public class KeyGeneratorDesSpi extends KeyGeneratorSpiImpl {
    
       Provider provider=null;
    
    public KeyGeneratorDesSpi() throws ClassNotFoundException
    {
    provider=ProviderToUse.LoadProvider();
    
    }
    
    
    @Override
    protected void engineInit(SecureRandom sr) {
        
        
      
       
//     if(provider.getName()=="SunJCE")
//        {
            try
            {
                
               KeyGenerator kg=KeyGenerator.getInstance("DES",ProviderToUse.ProiderName);
               
               kg.init(sr);
                
                
      
            }catch(Exception ex)
            {
                ex.printStackTrace();
            }
//            }
//        if(provider.getName()=="BC")
//        {
//            try
//            {
//                KeyGenerator kg= KeyGenerator.getInstance("DES",provider);
//                kg.init(sr);
//            }
//            catch(Exception ex)
//            {
//                ex.printStackTrace();
//            }
//            
//        }
    
    
    
    
    }

    @Override
    protected void engineInit(AlgorithmParameterSpec aps, SecureRandom sr) throws InvalidAlgorithmParameterException {
 
          try
        {
            
//            if(provider.getName()=="SunJCE")
//            {
            KeyGenerator kg= KeyGenerator.getInstance("DES",ProviderToUse.ProiderName);
            kg.init(aps, sr);
//            }
//          
//            if(provider.getName()=="BC")
//            {
//           KeyGenerator kg=KeyGenerator.getInstance("DES",provider);
//           kg.init(aps, sr);
//                
//            }
        }
       
        catch(Exception ex)
        {
            ex.printStackTrace();;
        }
        
        
    }

    @Override
    protected void engineInit(int i, SecureRandom sr) {
 
           try
        {
            
//            if(provider.getName()=="SunJCE")
//            {
            KeyGenerator kg= KeyGenerator.getInstance("DES",ProviderToUse.ProiderName);
            kg.init(i, sr);
//            }
//          
//            if(provider.getName()=="BC")
//            {
//           KeyGenerator kg=KeyGenerator.getInstance("DES",provider);
//           kg.init(i, sr);
//                
//            }
        }
       
        catch(Exception ex)
        {
            ex.printStackTrace();;
        }
        
        
        
        
    }

    @Override
    protected SecretKey engineGenerateKey() {
   SecretKey skei=null;
           try
        {
            
//            if(provider.getName()=="SunJCE")
//            {
            KeyGenerator kg= KeyGenerator.getInstance("DES",ProviderToUse.ProiderName);
             skei=kg.generateKey();
//            }
//          
//            if(provider.getName()=="BC")
//            {
//           KeyGenerator kg=KeyGenerator.getInstance("DES",provider);
//             skei=kg.generateKey();
//                
//            }
        }
       
        catch(Exception ex)
        {
            ex.printStackTrace();;
        }
        
        return skei;
    }
    
    
    
}
