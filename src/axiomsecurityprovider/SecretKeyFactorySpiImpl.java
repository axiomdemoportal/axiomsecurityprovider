/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

/**
 *
 * @author Sai
 */
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactorySpi;
import javax.crypto.SecretKeyFactory;
import java.security.Provider;
public class SecretKeyFactorySpiImpl extends SecretKeyFactorySpi{

    Provider provider=null;
    public SecretKeyFactorySpiImpl() throws ClassNotFoundException
    {
        provider=ProviderToUse.LoadProvider();
    }
    @Override
    protected SecretKey engineGenerateSecret(KeySpec ks) throws InvalidKeySpecException {
      
        SecretKey skei =null;
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
        SecretKeyFactory sf= SecretKeyFactory.getInstance("RSA",ProviderToUse.ProiderName);
       skei=     sf.generateSecret(ks);
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                SecretKeyFactory sf= SecretKeyFactory.getInstance("RSA",provider);
//            skei=    sf.generateSecret(ks);
//            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return skei;
    }

    @Override
    protected KeySpec engineGetKeySpec(SecretKey sk, Class type) throws InvalidKeySpecException {
      
          KeySpec skei =null;
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
          SecretKeyFactory sf= SecretKeyFactory.getInstance("RSA",ProviderToUse.ProiderName);
          skei=sf.getKeySpec(sk, type);
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                SecretKeyFactory sf= SecretKeyFactory.getInstance("RSA",provider);
//            skei=    sf.getKeySpec(sk, type);
//            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return skei;
        
        
        
    }

    @Override
    protected SecretKey engineTranslateKey(SecretKey sk) throws InvalidKeyException {
      
        SecretKey skei =null;
        try
        {
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//            {
        SecretKeyFactory sf= SecretKeyFactory.getInstance("RSA",ProviderToUse.ProiderName);
       skei= sf.translateKey(sk);
//            }
//            if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//            {
//                SecretKeyFactory sf= SecretKeyFactory.getInstance("RSA",provider);
//            skei= sf.translateKey(sk);
//            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return skei;
     
        
        
    }
    
    
    
    
}
