package axiomsecurityprovider;

import java.util.Date;
import java.util.Properties;

public class LoadSettings {

    public static Properties g_sSettings = null;
    public static String g_strPath = null;
    public static Properties g_templateSettings = null;
    public static String g_templatestrPath = null;
    public static Properties g_subjecttemplateSettings = null;
    public static String g_subjecttemplatestrPath = null;
 
    public static String g_strPath_bridge = null;
    public static Properties g_sSettings_bridge = null;
    public static Properties g_unitsSettings = null;
    public static Properties g_errorcodeSettings = null;
    public static String g_errorcodestrPath = null;
    public static Properties g_errorMessageSettings = null;
    public static String g_errorMessagestrPath = null;

    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
//usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
        //String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        //int iPRODUCT = (new Integer(strProductType)).intValue();
        int iPRODUCT = 3;
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        } else if (iPRODUCT == 1) {
            usrhome += sep + "dictumv2-settings";
        }

        g_subjecttemplatestrPath = usrhome + sep;
        String filepath = usrhome + sep + "subject.templates.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_subjecttemplateSettings = p1.properties;
            //System.out.println("subject template  file loaded >>" + filepath);
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "subject template file failed to load >> " + filepath);
        }

        String unitsfilepath = usrhome + sep + "units.conf";
        PropsFileUtil p2 = new PropsFileUtil();

        if (p2.LoadFile(unitsfilepath) == true) {
            g_unitsSettings = p2.properties;
            //System.out.println("subject template  file loaded >>" + filepath);
        } else {
            System.out.println("subject template  file failed to load >> " + filepath);
        }

    }

    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
//usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
        //usrhome += sep + "axiomv2-settings";
        //String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        //int iPRODUCT = (new Integer(strProductType)).intValue();
        int iPRODUCT = 3;
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        } else if (iPRODUCT == 1) {
            usrhome += sep + "dictumv2-settings";
        }

        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "dbsetting.conf";

        //filepath = "/Applications/NetBeans/apache-tomcat-7.0.27/axiomv2-settings/dbsetting.conf";
        PropsFileUtil p = new PropsFileUtil();

        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
//            System.out.println("dbsetting setting file loaded >>" + filepath);

        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "dbsetting setting file failed to load >> " + filepath);
        }
    }

    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
//        usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
        //usrhome += sep + "axiomv2-settings";
        //String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        //int iPRODUCT = (new Integer(strProductType)).intValue();
        int iPRODUCT = 3;
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        } else if (iPRODUCT == 1) {
            usrhome += sep + "dictumv2-settings";
        }
        g_templatestrPath = usrhome + sep;
        String filepath = usrhome + sep + "templates.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_templateSettings = p1.properties;
            //System.out.println("template  file loaded >>" + filepath);
        } else {
            System.out.println(new Date() + ">>" + "template  file failed to load >> " + filepath);
        }
    }
    
    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
//        usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
        //usrhome += sep + "axiomv2-settings";
        //String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        //int iPRODUCT = (new Integer(strProductType)).intValue();
        int iPRODUCT = 3;
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        } else if (iPRODUCT == 1) {
            usrhome += sep + "dictumv2-settings";
        }
        g_errorMessagestrPath
                = usrhome + sep;
        String filepath = usrhome + sep + "errormessages.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_errorMessageSettings = p1.properties;
            //System.out.println("template  file loaded >>" + filepath);
        } else {
            System.out.println(new Date() + ">>" + "Error file failed to load >> " + filepath);
        }
    }

    

    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
//        usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
        //usrhome += sep + "axiomv2-settings";
        //String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        //int iPRODUCT = (new Integer(strProductType)).intValue();
        int iPRODUCT = 3;
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        } else if (iPRODUCT == 1) {
            usrhome += sep + "dictumv2-settings";
        }
        g_errorcodestrPath = usrhome + sep;
        String filepath = usrhome + sep + "errorocode.errornames.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_errorcodeSettings = p1.properties;
            //System.out.println("template  file loaded >>" + filepath);
        } else {
            System.out.println(new Date() + ">>" + "Error file failed to load >> " + filepath);
        }
    }

    public static void LoadManually(String path) {
        DBSettingManual(path);
        TemplateSettingManual(path);
        SubjectTemplateSettingManual(path);
    }

    private static void DBSettingManual(String path) {
        String sep = System.getProperty("file.separator");
        String usrhome = path;
        //usrhome += sep + "axiomv2-settings";
        //String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        //int iPRODUCT = (new Integer(strProductType)).intValue();
        int iPRODUCT = 3;
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        } else if (iPRODUCT == 1) {
            usrhome += sep + "dictumv2-settings";
        }
        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "dbsetting.conf";
        PropsFileUtil p = new PropsFileUtil();

        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
//            System.out.println("manually loaded dbsetting setting file loaded >>" + filepath);

        } else {
            System.out.println(new Date() + ">>" + "manually loaded dbsetting setting file failed to load >> " + filepath);
        }
    }

    private static void TemplateSettingManual(String path) {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
//        usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
        //usrhome += sep + "axiomv2-settings";
        //String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        //int iPRODUCT = (new Integer(strProductType)).intValue();
        int iPRODUCT = 3;
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        } else if (iPRODUCT == 1) {
            usrhome += sep + "dictumv2-settings";
        }
        g_templatestrPath = usrhome + sep;
        String filepath = usrhome + sep + "templates.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_templateSettings = p1.properties;
//            System.out.println("manually loaded  template  file loaded >>" + filepath);
        } else {
            System.out.println(new Date() + ">>" + "manually loaded  template  file failed to load >> " + filepath);
        }
    }

    private static void SubjectTemplateSettingManual(String path) {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
//        usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
        //usrhome += sep + "axiomv2-settings";
        //String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        //int iPRODUCT = (new Integer(strProductType)).intValue();
        int iPRODUCT = 3;
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        } else if (iPRODUCT == 1) {
            usrhome += sep + "dictumv2-settings";
        }
        g_subjecttemplatestrPath = usrhome + sep;
        String filepath = usrhome + sep + "subject.templates.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_subjecttemplateSettings = p1.properties;
            //System.out.println("manually loaded  subject template  file loaded >>" + filepath);
        } else {
            System.out.println(new Date() + ">>" + "manually loaded  subject template  file failed to load >> " + filepath);
        }
    }

//    public static void LoadChannelProfile(ChannelProfile chprofile) {
//        gs_channelprofilesettting = chprofile;
//
//        String sep = System.getProperty("file.separator");
//        String usrhome = System.getProperty("catalina.home");
//    if(usrhome == null) { usrhome = System.getenv("catalina.home"); }
//        usrhome += sep + "axiomv2-settings";
//        g_strPath = usrhome + sep;
//        String filepath = usrhome + sep + "dbsetting.conf";
//        PropsFileUtil p = new PropsFileUtil();
//
//        if (p.LoadFile(filepath) == true) {
//            g_sSettings = p.properties;
////            System.out.println("dbsetting setting file loaded >>" + filepath);
//
//            if (gs_channelprofilesettting != null) {
//                g_sSettings.setProperty("remote.sign.archive", gs_channelprofilesettting.remotesignarchive);
//                g_sSettings.setProperty("bulk.email.attachment", gs_channelprofilesettting.bulkemailattachment);
//                g_sSettings.setProperty("cleanup.log", gs_channelprofilesettting.cleanuppath);
//
//                g_sSettings.setProperty("check.user", String.valueOf(gs_channelprofilesettting.checkUser));
//                g_sSettings.setProperty("user.alert", String.valueOf(gs_channelprofilesettting.alertUSer));
//
//                g_sSettings.setProperty("user.delete", String.valueOf(gs_channelprofilesettting.deleteUser));
//                g_sSettings.setProperty("user.edit", String.valueOf(gs_channelprofilesettting.editUser));
//                g_sSettings.setProperty("alert.media", gs_channelprofilesettting.alertmedia);
//
//                g_sSettings.setProperty("sw.otp.type", gs_channelprofilesettting.swotptype);
//                g_sSettings.setProperty("check.session", String.valueOf(gs_channelprofilesettting.checksession));
//                g_sSettings.setProperty("tokensetting.load", gs_channelprofilesettting.tokensettingload);
//
//                g_sSettings.setProperty("otp.specification", gs_channelprofilesettting.otpspecification);
//                g_sSettings.setProperty("cert.specification", gs_channelprofilesettting.certspecification);
//                g_sSettings.setProperty("sign.specification", gs_channelprofilesettting.signspecification);
//
//                g_sSettings.setProperty("location.classname", gs_channelprofilesettting.locationclassname);
//
//            }
//
//        } else {
//            System.out.println("dbsetting setting file failed to Set >> " + filepath);
//        }
//    }
//    
   

   

  static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }

//        usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
        String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
        int iPRODUCT = (new Integer(strProductType)).intValue();
        if (iPRODUCT == 3) {
            usrhome += sep + "axiomv2-settings";
        }

        g_strPath_bridge = usrhome + sep;
        String filepath = usrhome + sep + "axiombridgesetting.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_sSettings_bridge = p1.properties;
            //System.out.println("subject template  file loaded >>" + filepath);
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "bridge setting file failed to load >> " + filepath);
        }
    }

}
