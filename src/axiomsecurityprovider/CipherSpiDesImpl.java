/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherSpi;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

/**
 *
 * @author Sai
 */
public class CipherSpiDesImpl extends CipherSpi {
    public static String providerName=null;
    public static Provider provider;
    private static Cipher cf = null;
    private final static String PAD_NONE = "NoPadding";

    private final static String PAD_PKCS1 = "PKCS1Padding";

    private final static String PAD_OAEP_MGF1 = "OAEP";

    public CipherSpiDesImpl() {
        try {
            provider = ProviderToUse.LoadProvider();
             providerName=provider.getName();
          } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void engineSetMode(String string) throws NoSuchAlgorithmException {
        //ProviderToUse.LoadProvider();
        try {
//            if (provider.getName() == "SunJCE") {
//               cf = Cipher.getInstance("RSA", provider);
//
//            } else if (provider.getName() == "BC") {
//                cf = Cipher.getInstance("RSA", provider);
//
//                }
//            else if(provider.getName()=="SAFENET.3")
//            {
                 cf = Cipher.getInstance("RSA",providerName);
                
          //  }
            

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    @Override
    protected void engineSetPadding(String string) throws NoSuchPaddingException {

        try {
//            if (provider.getName() == "SunJCE") {
//                cf = Cipher.getInstance("RSA", provider);
//
//            } else if (provider.getName() == "BC") {
                cf = Cipher.getInstance("RSA",providerName);

          //  }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    @Override
    protected int engineGetBlockSize() {
        int x = 0;
        try {
//            if (provider.getName() == "SunJCE") {
//              //  cf = Cipher.getInstance("RSA", provider);
//                x = cf.getBlockSize();
//            } else if (provider.getName() == "BC") {
            //  cf = Cipher.getInstance("RSA", provider);
                x = cf.getBlockSize();

           // }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return x;

    }

    @Override
    protected int engineGetOutputSize(int i) {
        int x = 0;
        try {
//            if (provider.getName() == "SunJCE") {
//              //  cf = Cipher.getInstance("RSA", provider);
//                x = cf.getOutputSize(i);
//            } else if (provider.getName() == "BC") {
           //  cf = Cipher.getInstance("RSA", provider);
                x = cf.getOutputSize(i);

          //  }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return x;

    }

    @Override
    protected byte[] engineGetIV() {
        byte[] ic = null;
        try {
//            if (provider.getName() == "SunJCE") {
//          
//               //  cf = Cipher.getInstance("RSA", provider);
//                ic = cf.getIV();
//            } else if (provider.getName() == "BC") {
             
              // cf = Cipher.getInstance("RSA", provider);
                ic = cf.getIV();

           // }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return ic;

    }

    @Override
    protected AlgorithmParameters engineGetParameters() {
        AlgorithmParameters ap = null;
        try {
//            if (provider.getName() == "SunJCE") {
//              //  cf = Cipher.getInstance("RSA", provider);
//                ap = cf.getParameters();
//            } else if (provider.getName() == "BC") {
//               // cf = Cipher.getInstance("RSA", provider);
                ap = cf.getParameters();

         //   }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return ap;
    }

    @Override
    protected void engineInit(int i, Key key, SecureRandom sr) {
        try {
            System.out.println(provider.getName());
//            if ((provider.getName()).equals("SunJCE")) {
//                cf = Cipher.getInstance("RSA",providerName);
//                cf.init(i, key, sr);
//            } else if (provider.getName() == "BC") {
//                System.out.println(provider.getName());
                cf = Cipher.getInstance("RSA", providerName);
                cf.init(i, key, sr);

           // }

        } catch (Exception ex) {
            System.out.println(ex + "bad");
        }

    }

    @Override
    protected void engineInit(int i, Key key, AlgorithmParameterSpec aps, SecureRandom sr) {

        try {
//            if (provider.getName() == "SunJCE") {
//               // cf = Cipher.getInstance("RSA", provider);
//                cf.init(i, key, aps, sr);
//
//            } else if (provider.getName() == "BC") {
               // cf = Cipher.getInstance("RSA", provider);
                cf.init(i, key, aps, sr);

          //  }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    @Override
    protected void engineInit(int i, Key key, AlgorithmParameters ap, SecureRandom sr) throws InvalidKeyException, InvalidAlgorithmParameterException {
        try {
//            if (provider.getName() == "SunJCE") {
//
//              // cf = Cipher.getInstance("RSA", provider);
//
//                cf.init(i, key, ap, sr);
//            } else if (provider.getName() == "BC") {
             //  cf= Cipher.getInstance("RSA", provider);
                cf.init(i, key, ap, sr);

           // }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    @Override
    protected byte[] engineUpdate(byte[] bytes, int i, int i1) {
        byte[] op = null;
        try {
//            if (provider.getName() == "SunJCE") {
//              //cf = Cipher.getInstance("RSA", provider);
//                op = cf.update(bytes, i, i1);
//            } else if (provider.getName() == "BC") {
               // cf= Cipher.getInstance("RSA", provider);
                op = cf.update(bytes, i, i1);

          //  }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return op;

    }

    @Override
    protected int engineUpdate(byte[] bytes, int i, int i1, byte[] bytes1, int i2) throws ShortBufferException {
        int op = 0;
        try {
//            if (provider.getName() == "SunJCE") {
//               // cf = Cipher.getInstance("RSA", provider);
//                op = cf.update(bytes, i, i1, bytes1, i2);
//            } else if (provider.getName() == "BC") {
              //cf = Cipher.getInstance("RSA", provider);
                op = cf.update(bytes, i, i1, bytes1, i2);

        //    }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return op;

    }

    @Override
    protected byte[] engineDoFinal(byte[] bytes, int i, int i1) throws IllegalBlockSizeException, BadPaddingException {

        byte[] op = null;
        try {
//            if (provider.getName() == "SunJCE") {
//               // cf = Cipher.getInstance("RSA", provider);
//                op = cf.doFinal(bytes, i, i1);
//            } else if (provider.getName() == "BC") {
               // cf = Cipher.getInstance("RSA", provider);
                
                op = cf.doFinal(bytes, i, i1);

          //  }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return op;

    }

    @Override
    protected int engineDoFinal(byte[] bytes, int i, int i1, byte[] bytes1, int i2) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
        int op = 0;
        try {
//            if (provider.getName() == "SunJCE") {
//              // cf= Cipher.getInstance("RSA", provider);
//                op = cf.update(bytes, i, i1, bytes1, i2);
//            } else if (provider.getName() == "BC") {
               // cf = Cipher.getInstance("RSA", provider);
                op = cf.update(bytes, i, i1, bytes1, i2);

           // }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        return op;

    }
    
    
}
