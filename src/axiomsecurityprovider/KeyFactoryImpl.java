/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package axiomsecurityprovider;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyFactorySpi;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.Provider;
/**
 *
 * @author Sai
 */
public class KeyFactoryImpl  extends KeyFactorySpi{

    Provider provider=null;
  
    public KeyFactoryImpl() throws ClassNotFoundException
            
    {
        provider=ProviderToUse.LoadProvider();
    }
    
    
    @Override
    protected PublicKey engineGeneratePublic(KeySpec keySpec) throws InvalidKeySpecException {
        PublicKey k=null;
//        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//        {
          try
            {
            KeyFactory kf= KeyFactory.getInstance("RSA",ProviderToUse.ProiderName);
           PublicKey bobPubKey = kf.generatePublic(keySpec);
            k=bobPubKey;
//            }catch(Exception ex)
//            {
//                ex.printStackTrace();
//            }
            
//        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//        {
//            try
//            {
//          KeyFactory kf= KeyFactory.getInstance("RSA",provider);
//           PublicKey bobPubKey = kf.generatePublic(keySpec);
//              k=bobPubKey;
//            }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
              return k;
        }
 

    @Override
    protected PrivateKey engineGeneratePrivate(KeySpec keySpec) throws InvalidKeySpecException {
        
        PrivateKey pk=null;
     
          try
          {
          KeyFactory kf=KeyFactory.getInstance("RSA",ProviderToUse.ProiderName);
         PrivateKey bobPubKey = kf.generatePrivate(keySpec);
            pk=bobPubKey;
          }
         
      catch(Exception ex)
          {
              ex.printStackTrace();
          }
          return pk;
        
    
    }
      
        
        
    

    @Override
    protected <T extends KeySpec> T engineGetKeySpec(Key key, Class<T> keySpec) throws InvalidKeySpecException {
        T  spec=null;
          try
         {
        KeyFactory kf=KeyFactory.getInstance("RSA",ProviderToUse.ProiderName);
        spec=kf.getKeySpec(key, keySpec);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
     
        
        return spec;
        
        
    }

    @Override
    protected Key engineTranslateKey(Key key) throws InvalidKeyException {
       
        Key k=null;
//        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="JCE")
//        {
            try
            {
            KeyFactory kf=KeyFactory.getInstance("RSA",ProviderToUse.ProiderName);
           k= kf.translateKey(key);
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
           
//        }
//        if(axiomsecurityprovider.AxiomSecurityProvider.SelectedProvider=="BC")
//        {
//            try
//            {
//            KeyFactory kf=KeyFactory.getInstance("RSA",provider);
//           k= kf.translateKey(key);
//            }
//            catch(Exception ex)
//            {
//                ex.printStackTrace();
//            }
//            
//            
//        }
        
        return k;
       
    }
    
}
